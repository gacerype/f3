#!bin/bash

#1
wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
dpkg -i gitlab-runner_*.deb
sudo chmod +x /usr/local/bin/gitlab-runner


#2 working fine
sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
sudo chmod +x /usr/local/bin/gitlab-runner
sudo usermod -aG docker gitlab-runner
sudo service docker restart

#turnoff Shared runners on site

sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/ype/gitlab-runner
sudo gitlab-runner start
sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
sudo gitlab-runner register

sudo gitlab-runner --version
sudo gitlab-runner status
sudo gitlab-runner list
sudo gitlab-runner verify

